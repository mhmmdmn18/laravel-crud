@extends('adminlte.master')

@section('title')
	Add Cast
@endsection

@section('content')
	<div>
		<form action="/cast" method="POST">
			@csrf
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
				@error('name')
					<div class="alert alert-danger">
						{{ $message }}
					</div>
				@enderror
			</div>
			<div class="form-group">
				<label for="age">Age</label>
				<input type="number" class="form-control" name="age" id="age" placeholder="Enter Age">
				@error('age')
					<div class="alert alert-danger">
						{{ $message }}
					</div>
				@enderror
			</div>
			<div class="form-group">
				<label for="bio">Biography</label>
				<textarea class="form-control" name="bio" id="bio" placeholder="Enter Biography"></textarea>
				@error('bio')
					<div class="alert alert-danger">
						{{ $message }}
					</div>
				@enderror
			</div>
			<button type="submit" class="btn btn-primary">Add</button>
		</form>
	</div>
@endsection