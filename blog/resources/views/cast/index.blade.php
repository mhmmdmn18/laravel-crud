@extends('adminlte.master')

@push('style')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.25/datatables.min.css"/>
@endpush

@section('title')
	View Casts
@endsection

@section('content')
	<div>
		<a href="/cast/create" class="btn btn-primary mb-3 px-3">Add Cast</a>
		<table id="casts" class="table table-bordered">
			<thead class="thead-light">
			  	<tr>
					<th class="col-1">#</th>
					<th class="col-3">Name</th>
					<th class="col-1">Age</th>
					<th class="col-4">Biography</th>
					<th class="col-3">Actions</th>
			  	</tr>
			</thead>
			<tbody>
				@forelse ($cast as $key=>$value)
					<tr> 
						<td>{{ $key + 1 }}</td>
						<td>{{ $value->name }}</td>
						<td>{{ $value->age }}</td>
						<td>{{ $value->bio }}</td>
						<td>
							<form action="/cast/{{ $value->id }}" method="POST">
							<a href="/cast/{{ $value->id }}" class="btn btn-success">Show</a>
							<a href="/cast/{{ $value->id }}/edit" class="btn btn-warning">Edit</a>
								@csrf
								@method('DELETE')
								<input type="submit" class="btn btn-danger my-1" value="Delete">
							</form>
						</td>
					</tr>
				@empty
				<tr>
					<td>No data</td>
				</tr>
				@endforelse              
			</tbody>
		</table>
	</div>
@endsection

@push('script')
	<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
	<script>
		$(function () {
		$("#casts").DataTable();
		});
	</script>
@endpush