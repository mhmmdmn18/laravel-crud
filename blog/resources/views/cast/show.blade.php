@extends('adminlte.master')

@section('title')
	Show Cast {{ $cast->id }}
@endsection

@section('content')
	<div>
		<h3>{{ $cast->name }}</h3>
		<img src="#" alt="photo">
		<hr>
		<p>{{ $cast->bio }}.... <a href="#">See full bio </a><small>>></small></p>
		<p><b>Age:</b> {{ $cast->age }}</p>
	</div>
@endsection
